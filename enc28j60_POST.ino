#include <EtherCard.h>

#define PATH    "POST-data.php"


// ethernet interface mac address, must be unique on the LAN
byte mymac[] = { 0x74, 0x69, 0x69, 0x2D, 0x30, 0x31 };

const char website[] PROGMEM = "your website";//if you have one

byte Ethernet::buffer[700];
uint32_t timer;
Stash stash;

void setup () {
  Serial.begin(57600);
  Serial.println("POST Data");

  if (ether.begin(sizeof Ethernet::buffer, mymac, 10) == 0)
    Serial.println( "Failed to access Ethernet controller");
  if (!ether.dhcpSetup())
    Serial.println("DHCP failed");

  ether.printIp("IP:  ", ether.myip);
  ether.printIp("GW:  ", ether.gwip);
  ether.printIp("DNS: ", ether.dnsip);

  if (!ether.dnsLookup(website))
    Serial.println("DNS failed");
  ether.parseIp (ether.hisip, "192.168.0.105");// or a local server

  ether.printIp("SRV: ", ether.hisip);
}

void loop () {
  ether.packetLoop(ether.packetReceive());

  if (millis() > timer) {
    timer = millis() + 60000;
    byte val = random(255);
    String val1 = String(val);
    val = random(100) * analogRead(A2) / 4;
    String val2 = String(val);
    byte sd = stash.create();
    stash.print("temp=");
    stash.print(val1);
    stash.print("&");
    stash.print("temp2=");
    stash.print(val2);
    stash.save();

    Stash::prepare(PSTR("POST /$F HTTP/1.1" "\r\n"
                        "Host: $F" "\r\n"
                        "Content-Length: $D" "\r\n"
                        "Content-Type: application/x-www-form-urlencoded" "\r\n"
                        "\r\n"
                        "$H"),
                   PSTR(PATH), website, stash.size(), sd);

    // send the packet - this also releases all stash buffers once done
    ether.tcpSend();

  }
}
