<?php
	
	define("LOG_FILE", "./data.csv");
	require_once('./jpgraph/src/jpgraph.php');
	require_once('./jpgraph/src/jpgraph_line.php');
	
	$times = array();
	$values1 = array();	
	$values2=array();
	$file_lines = file(LOG_FILE, FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
	//Get the data
	foreach($file_lines as $line_num => $line_value) {
		$line_elements = explode(",", $line_value);
		#$times[] = date("H:i:s", $line_elements[0]);
		$times[] = date("d-m-y H:i", $line_elements[0]);
		$values1[] = $line_elements[1];
		$values2[] = $line_elements[2];
	}
	// setup the graph
	$graph = new Graph(1100, 550);
	$graph->SetFrame(false);
	#$graph->SetScale('intint');
	$graph->SetScale('textlin');
	$graph->SetShadow($aShowShadow=true,$aShadowWidth=5,$aShadowColor=array(102,102,102));
	$graph->ygrid->SetFill(true,'#EFEFEF@0.5','#BBCCFF@0.5');
	$graph->ygrid->Show();
	$graph->xgrid->Show();


   #$graph->img->SetAntiAliasing(false);
    //li,re,bo/on
    $graph->SetMargin(50,30,36,90);
    $graph->SetMarginColor('white');
	
    //Setup Title and axis
	$graph->title->Set("Temperatures");
	$graph->xaxis->SetLabelAngle('50');
	$graph->xaxis->SetTickLabels($times);
	$graph->xaxis->SetLabelSide(SIDE_BOTTOM);

	$graph->yaxis->scale->SetAutoMin(0);
	$graph->yaxis->title->Set("&deg;C");
	
	$graph->ygrid->SetFill($aFlg=true, $aColor1='white', $aColor2='gray9');
    // Create the first line
	$lineplot = new LinePlot($values1);
    $lineplot->SetLegend('Living Room');
	#https://jpgraph.net/features/src/show-example.php?target=new_line1.php
	$graph->Add($lineplot);
	$lineplot->SetColor("green");

    // Create the second line
    $p2 = new LinePlot($values2);
    $graph->Add($p2);
    $p2->SetColor("#FF1493");
    $p2->SetLegend('Bedroom');

    $graph->legend->SetFrameWeight(1);
	// Finally send the graph to the browser
	$graph->Stroke();
?>
